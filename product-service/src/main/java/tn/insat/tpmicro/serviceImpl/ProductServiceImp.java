package tn.insat.tpmicro.serviceImpl;

import java.util.List;

import lombok.RequiredArgsConstructor;
import tn.insat.tpmicro.domain.Product;
import tn.insat.tpmicro.repository.ProductRepository;
import tn.insat.tpmicro.service.ProductService;

@RequiredArgsConstructor
public class ProductServiceImp implements ProductService {

	private final ProductRepository productRepository;

	@Override
	public Product addProduct(Product product) {
		return productRepository.save(product);
	}

	@Override
	public List<Product> getProducts() {
		return productRepository.findAll();
	}

}
