package tn.insat.tpmicro.service;

import java.util.List;

import org.springframework.stereotype.Service;

import tn.insat.tpmicro.domain.Product;

@Service
public interface ProductService {

	public Product addProduct(Product product);

	public List<Product> getProducts();

}
