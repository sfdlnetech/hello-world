package tn.insat.tpmicro.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import tn.insat.tpmicro.domain.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

}
