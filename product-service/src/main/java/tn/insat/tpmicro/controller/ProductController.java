package tn.insat.tpmicro.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import tn.insat.tpmicro.domain.Product;
import tn.insat.tpmicro.service.ProductService;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/products")
public class ProductController {

	private final ProductService productService;

	@GetMapping("products")
	public List<Product> getProducts() {
		return productService.getProducts();
	}

}
