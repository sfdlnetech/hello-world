package tn.insat.tpmicro.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import tn.insat.tpmicro.DTO.ProductDTO;
import tn.insat.tpmicro.domain.Product;

@Mapper
public interface ProductMapper {
	ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

	ProductDTO productToProductDTO(Product product);
}
